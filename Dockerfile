FROM golang:1.19-alpine

COPY . /app
WORKDIR /app

RUN apk add --no-cache openssh &&\
    eval $(ssh-agent -s) &&\
    cat ./vk-edu-golang-g-simonyan-nJrcjoFY.pem | tr -d '\r' | ssh-add - &&\
    mkdir -p ~/.ssh &&\
    chmod 700 ~/.ssh &&\
    ssh-keyscan -t rsa 212.233.91.182 >> ~/.ssh/known_hosts &&\
    scp -r ./configs centos@212.233.91.182:/home/centos/server &&\
    scp ./docker-compose.yml centos@212.233.91.182:/home/centos/server

RUN apk add --no-cache make &&\
    make build

EXPOSE 8080/tcp

CMD [ "./bin/server" ]