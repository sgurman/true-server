APP_NAME = server

.PHONY: build
build:
	go build -mod=vendor -v -o ./bin/${APP_NAME} ./cmd/${APP_NAME}

.PHONY: docker
docker:
	docker compose build --pull
	docker compose push