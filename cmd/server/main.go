package main

import (
	"fmt"
	"os"

	"gitlab.com/sgurman/true-server/internal/api/middleware"
	commenthttp "gitlab.com/sgurman/true-server/internal/pkg/comment/handler"
	commentrepo "gitlab.com/sgurman/true-server/internal/pkg/comment/repository"
	commentsvc "gitlab.com/sgurman/true-server/internal/pkg/comment/service"
	"gitlab.com/sgurman/true-server/internal/pkg/metric"
	"gitlab.com/sgurman/true-server/internal/pkg/session"
	threadhttp "gitlab.com/sgurman/true-server/internal/pkg/thread/handler"
	threadrepo "gitlab.com/sgurman/true-server/internal/pkg/thread/repository"
	threadsvc "gitlab.com/sgurman/true-server/internal/pkg/thread/service"

	echoprom "github.com/labstack/echo-contrib/prometheus"
	echo "github.com/labstack/echo/v4"
	echomiddleware "github.com/labstack/echo/v4/middleware"
	echolog "github.com/labstack/gommon/log"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func main() {
	encoderCfg := zap.NewProductionEncoderConfig()
	encoderCfg.EncodeTime = zapcore.ISO8601TimeEncoder
	zapLogger := zap.New(zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		zapcore.Lock(os.Stdout),
		zap.NewAtomicLevel(),
	))

	defer zapLogger.Sync() // nolint:errcheck
	logger := zapLogger.Sugar()

	e := echo.New()
	p := echoprom.NewPrometheus("echo", nil, []*echoprom.Metric{
		metric.Hits,
		metric.Timings,
		metric.ExternalHits,
		metric.ExternalTimings,
	})
	sessionSvc := session.NewService(logger)

	p.Use(e)
	e.Use(echomiddleware.RecoverWithConfig(echomiddleware.RecoverConfig{
		StackSize: 1 << 10, // 1 KB
		LogLevel:  echolog.ERROR,
	}))
	e.Use(middleware.ForwardReqIDMiddleware())
	e.Use(middleware.AccessLogMiddleware(logger))
	e.Use(middleware.AuthEchoMiddleware(sessionSvc, logger))

	threadRepo := threadrepo.NewRepository(logger)
	threadSvc := threadsvc.NewService(threadRepo)
	threadHandler := threadhttp.Handler{ThreadSvc: threadSvc, Logger: logger}

	commentRepo := commentrepo.NewRepository(logger)
	commentSvc := commentsvc.NewService(commentRepo, threadRepo)
	commentHandler := commenthttp.Handler{CommentSvc: commentSvc, Logger: logger}

	e.GET("/thread/:tid", threadHandler.GetThread)
	e.POST("/thread", threadHandler.CreateThread)
	e.POST("/thread/:tid/comment", commentHandler.Create)
	e.POST("/thread/:tid/comment/:cid/like", commentHandler.Like)

	e.HideBanner = true
	fmt.Println(e.Start(":8080"))
}
