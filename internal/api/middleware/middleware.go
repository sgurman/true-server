package middleware

import (
	"crypto/rand"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/sgurman/true-server/internal/pkg/domain"
	"gitlab.com/sgurman/true-server/internal/pkg/metric"

	"github.com/labstack/echo/v4"
	"go.uber.org/zap"
)

var (
	skipUrls = map[string]bool{
		"/favicon.ico": true,
		"/metrics":     true,
		"/":            true,
	}
)

func getRandID() string {
	randID := make([]byte, 12)
	rand.Read(randID) //nolint:errcheck
	return fmt.Sprintf("%x", randID)
}

func ForwardReqIDMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			if skipUrls[context.Request().URL.Path] {
				return next(context)
			}
			if _, ok := context.Get("request_id").(string); !ok {
				context.Set("request_id", getRandID())
			}
			return next(context)
		}
	}
}

func AccessLogMiddleware(
	logger *zap.SugaredLogger,
) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			r := context.Request()
			path := r.URL.Path
			if skipUrls[path] {
				return next(context)
			}

			start := time.Now()
			err := next(context)

			total := time.Since(start)
			status := context.Response().Status

			metric.IncomingUpdate(
				strconv.Itoa(status),
				path,
				total.Seconds(),
			)

			logger.Infow("New Request",
				"remote_addr", r.RemoteAddr,
				"request_id", context.Get("request_id").(string),
				"method", r.Method,
				"url", path,
				"time", total.String(),
				"status", status,
			)
			fmt.Print("\n")
			return err
		}
	}
}

func AuthEchoMiddleware(
	service domain.SessionService,
	logger *zap.SugaredLogger,
) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			r := context.Request()
			if skipUrls[r.URL.Path] {
				return next(context)
			}

			cookie := http.Cookie{
				Name:    "user",
				Value:   "user_1",
				Expires: time.Now().Add(1 * time.Second),
			}
			r.AddCookie(&cookie)

			_, err := service.CheckSession(r.Header)
			if err != nil {
				logger.Infow("Service CheckSession err",
					"request_id", context.Get("request_id").(string),
					"err", err.Error(),
				)
				return context.NoContent(401)
			}
			return next(context)
		}
	}
}
