package handler

import (
	"gitlab.com/sgurman/true-server/internal/pkg/domain"

	"github.com/labstack/echo/v4"
	"go.uber.org/zap"
)

type Handler struct {
	CommentSvc domain.CommentService
	Logger     *zap.SugaredLogger
}

func (h Handler) Create(ctx echo.Context) error {
	var comment domain.Comment
	err := ctx.Bind(&comment)
	if err != nil {
		h.Logger.Errorw("Bind comment err",
			"request_id", ctx.Get("request_id").(string),
			"err", err.Error(),
		)
		return err
	}

	tid := ctx.Param("tid")

	err = h.CommentSvc.Create(tid, comment)
	if err != nil {
		h.Logger.Errorw("CommentSvc Create err",
			"request_id", ctx.Get("request_id").(string),
			"thread_id", tid,
			"comment", comment,
			"err", err.Error(),
		)
	}

	return err
}

func (h Handler) Like(ctx echo.Context) error {
	tid := ctx.Param("tid")
	cid := ctx.Param("cid")

	err := h.CommentSvc.Like(tid, cid)
	if err != nil {
		h.Logger.Errorw("CommentSvc Like err",
			"request_id", ctx.Get("request_id").(string),
			"thread_id", tid,
			"comment_id", cid,
			"err", err.Error(),
		)
	}

	return err
}
