package repository

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/sgurman/true-server/internal/pkg/domain"
	"gitlab.com/sgurman/true-server/internal/pkg/metric"

	"go.uber.org/zap"
)

type repository struct {
	log *zap.SugaredLogger
}

func NewRepository(logger *zap.SugaredLogger) domain.CommentRepository {
	return repository{log: logger}
}

func (r repository) Create(comment domain.Comment) error {
	reqBody, err := json.Marshal(comment)
	if err != nil {
		return fmt.Errorf("comment encode err: %w", err)
	}

	req, err := http.NewRequest(
		http.MethodPost,
		// "http://vk-golang.ru:16000/comment",
		"http://vk-golang.ru:16000/comment?fast=true",
		bytes.NewBuffer(reqBody),
	)
	if err != nil {
		return fmt.Errorf("new request err: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	url := req.URL.String()
	start := time.Now()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("do request err: %w", err)
	}
	defer resp.Body.Close()

	total := time.Since(start)
	status := resp.StatusCode

	metric.ExternalUpdate(
		strconv.Itoa(status),
		url,
		total.Seconds(),
	)

	r.log.Infow("External Access",
		"method", req.Method,
		"url", url,
		"body", string(reqBody),
		"time", total.String(),
		"status", status,
	)

	if status != 200 {
		return fmt.Errorf("failed to create comment remotely")
	}

	return nil
}

func (r repository) Like(commentID string) error {
	req, err := http.NewRequest(
		http.MethodPost,
		// fmt.Sprintf("http://vk-golang.ru:16000/comment/like?cid=%s", commentID),
		fmt.Sprintf("http://vk-golang.ru:16000/comment/like?superstable=true&cid=%s", commentID),
		nil,
	)
	if err != nil {
		return fmt.Errorf("new request err: %w", err)
	}

	url := req.URL.String()
	start := time.Now()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("do request err: %w", err)
	}
	defer resp.Body.Close()

	total := time.Since(start)
	status := resp.StatusCode

	metric.ExternalUpdate(
		strconv.Itoa(status),
		url,
		total.Seconds(),
	)

	r.log.Infow("External Access",
		"method", req.Method,
		"url", url,
		"time", total.String(),
		"status", status,
	)

	if status != 200 {
		return fmt.Errorf("failed to like comment remotely")
	}

	return nil
}
