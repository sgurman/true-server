package service

import (
	"fmt"

	"gitlab.com/sgurman/true-server/internal/pkg/domain"
)

type service struct {
	CommentRepo domain.CommentRepository
	ThreadRepo  domain.ThreadRepository
}

func NewService(
	commentRepo domain.CommentRepository,
	threadRepo domain.ThreadRepository,
) domain.CommentService {
	return service{
		CommentRepo: commentRepo,
		ThreadRepo:  threadRepo,
	}
}

func (s service) Create(threadID string, comment domain.Comment) error {
	if err := s.checkThread(threadID); err != nil {
		return err
	}
	err := s.CommentRepo.Create(comment)
	if err != nil {
		err = fmt.Errorf("repo create comm err: %w", err)
	}
	return err
}

func (s service) Like(threadID string, commentID string) error {
	if err := s.checkThread(threadID); err != nil {
		return err
	}
	err := s.CommentRepo.Like(commentID)
	if err != nil {
		err = fmt.Errorf("repo like comm err: %w", err)
	}

	return err
}

func (s service) checkThread(threadID string) error {
	_, err := s.ThreadRepo.Get(threadID)
	if err != nil {
		err = fmt.Errorf("check thread err: %w", err)
	}
	return err
}
