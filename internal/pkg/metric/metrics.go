package metric

import (
	echoprom "github.com/labstack/echo-contrib/prometheus"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	Hits = &echoprom.Metric{
		Name:        "hits",
		Description: "Incoming http requests' hit counter vector.",
		Type:        "counter_vec",
		Args:        []string{"status", "path"},
	}

	Timings = &echoprom.Metric{
		Name:        "timings",
		Description: "Incoming http requests' timing summary vector.",
		Type:        "summary_vec",
		Args:        []string{"path"},
	}

	ExternalHits = &echoprom.Metric{
		Name:        "external_hits",
		Description: "External requests' hit counter vector.",
		Type:        "counter_vec",
		Args:        []string{"status", "url"},
	}

	ExternalTimings = &echoprom.Metric{
		Name:        "external_timings",
		Description: "External requests' timing summary vector.",
		Type:        "summary_vec",
		Args:        []string{"url"},
	}
)

func IncomingUpdate(status, path string, totalSeconds float64) {
	Hits.MetricCollector.(*prometheus.CounterVec).
		WithLabelValues(status, path).
		Inc()
	Timings.MetricCollector.(*prometheus.SummaryVec).
		WithLabelValues(path).
		Observe(totalSeconds)
}

func ExternalUpdate(status, url string, totalSeconds float64) {
	ExternalHits.MetricCollector.(*prometheus.CounterVec).
		WithLabelValues(status, url).
		Inc()
	ExternalTimings.MetricCollector.(*prometheus.SummaryVec).
		WithLabelValues(url).
		Observe(totalSeconds)
}
