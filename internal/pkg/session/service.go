package session

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/sgurman/true-server/internal/pkg/domain"
	"gitlab.com/sgurman/true-server/internal/pkg/metric"

	"go.uber.org/zap"
)

type service struct {
	log *zap.SugaredLogger
}

func NewService(logger *zap.SugaredLogger) domain.SessionService {
	return service{log: logger}
}

func (s service) CheckSession(headers http.Header) (domain.Session, error) {
	req, err := http.NewRequest(
		http.MethodGet,
		// "http://vk-golang.ru:17000/int/CheckSession",
		"http://vk-golang.ru:17000/int/CheckSession?stable=true&light=true",
		nil,
	)
	if err != nil {
		return domain.Session{}, fmt.Errorf("new request err: %w", err)
	}

	req.Header = headers
	url := req.URL.String()
	start := time.Now()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return domain.Session{}, fmt.Errorf("do request err: %w", err)
	}
	defer resp.Body.Close()

	total := time.Since(start)
	status := resp.StatusCode

	metric.ExternalUpdate(
		strconv.Itoa(status),
		url,
		total.Seconds(),
	)

	s.log.Infow("External Access",
		"method", req.Method,
		"url", url,
		"time", total.String(),
		"status", status,
	)

	switch status {
	case 500:
		return domain.Session{}, fmt.Errorf("failed to request check session")
	case 200:
		return domain.Session{}, nil
	default:
		return domain.Session{}, domain.ErrNoSession
	}
}
