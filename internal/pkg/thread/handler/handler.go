package handler

import (
	"gitlab.com/sgurman/true-server/internal/pkg/domain"

	"github.com/labstack/echo/v4"
	"go.uber.org/zap"
)

type Handler struct {
	ThreadSvc domain.ThreadService
	Logger    *zap.SugaredLogger
}

func (h Handler) GetThread(ctx echo.Context) error {
	tid := ctx.Param("tid")

	t, err := h.ThreadSvc.Get(tid)
	if err != nil {
		h.Logger.Errorw("ThreadSvc Get err",
			"request_id", ctx.Get("request_id").(string),
			"thread_id", tid,
			"err", err.Error(),
		)
		return err
	}

	return ctx.JSON(200, t)
}

func (h Handler) CreateThread(ctx echo.Context) error {
	var thread domain.Thread
	err := ctx.Bind(&thread)
	if err != nil {
		h.Logger.Errorw("Bind thread err",
			"request_id", ctx.Get("request_id").(string),
			"err", err.Error(),
		)
		return err
	}

	err = h.ThreadSvc.Create(thread)
	if err != nil {
		h.Logger.Errorw("ThreadSvc Create err",
			"request_id", ctx.Get("request_id").(string),
			"err", err.Error(),
		)
		return err
	}

	return ctx.NoContent(200)
}
