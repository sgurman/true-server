package repository

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/sgurman/true-server/internal/pkg/domain"
	"gitlab.com/sgurman/true-server/internal/pkg/metric"

	"go.uber.org/zap"
)

type repository struct {
	log *zap.SugaredLogger
}

func NewRepository(logger *zap.SugaredLogger) domain.ThreadRepository {
	return repository{log: logger}
}

func (r repository) Create(thread domain.Thread) error {
	reqBody, err := json.Marshal(thread)
	if err != nil {
		return fmt.Errorf("thread encode err: %w", err)
	}

	req, err := http.NewRequest(
		http.MethodPost,
		// "http://vk-golang.ru:15000/thread",
		"http://vk-golang.ru:15000/thread?stable=true",
		bytes.NewBuffer(reqBody),
	)
	if err != nil {
		return fmt.Errorf("new request err: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	url := req.URL.String()
	start := time.Now()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("do request err: %w", err)
	}
	defer resp.Body.Close()

	total := time.Since(start)
	status := resp.StatusCode

	metric.ExternalUpdate(
		strconv.Itoa(status),
		url,
		total.Seconds(),
	)

	r.log.Infow("External access",
		"method", req.Method,
		"url", url,
		"body", string(reqBody),
		"time", total.String(),
		"status", status,
	)

	if status != 200 {
		return fmt.Errorf("failed to create thread remotely")
	}

	return nil
}

func (r repository) Get(id string) (domain.Thread, error) {
	req, err := http.NewRequest(
		http.MethodGet,
		// fmt.Sprintf("http://vk-golang.ru:15000/thread?id=%s", id),
		fmt.Sprintf("http://vk-golang.ru:15000/thread?thread_fast=true&id=%s", id),
		nil,
	)
	if err != nil {
		return domain.Thread{}, fmt.Errorf("new request err: %w", err)
	}

	url := req.URL.String()
	start := time.Now()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return domain.Thread{}, fmt.Errorf("do request err: %w", err)
	}
	defer resp.Body.Close()

	total := time.Since(start)
	status := resp.StatusCode

	metric.ExternalUpdate(
		strconv.Itoa(status),
		url,
		total.Seconds(),
	)

	r.log.Infow("External Access",
		"method", req.Method,
		"url", url,
		"time", total.String(),
		"status", status,
	)

	if status != 200 {
		return domain.Thread{}, fmt.Errorf("failed to fetch thread remotely")
	}

	var thread domain.Thread
	err = json.NewDecoder(resp.Body).Decode(&thread)
	if err != nil {
		return domain.Thread{}, fmt.Errorf("thread decode error: %w", err)
	}

	return thread, nil
}
