package service

import (
	"fmt"

	"gitlab.com/sgurman/true-server/internal/pkg/domain"
)

type service struct {
	ThreadRepo domain.ThreadRepository
}

func NewService(threadRepo domain.ThreadRepository) domain.ThreadService {
	return service{ThreadRepo: threadRepo}
}

func (s service) Create(thread domain.Thread) error {
	err := s.ThreadRepo.Create(thread)
	if err != nil {
		err = fmt.Errorf("repo create thread err: %w", err)
	}
	return err
}

func (s service) Get(id string) (domain.Thread, error) {
	thread, err := s.ThreadRepo.Get(id)
	if err != nil {
		err = fmt.Errorf("repo get thread err: %w", err)
	}
	return thread, err
}
